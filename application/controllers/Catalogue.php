<?php

class Catalogue extends CI_Controller {

    public function index()
    {
        $this->load->model('Catalogue_model');
        $recup = $this->Catalogue_model->recuptous();
        $data['tous']=$recup;
        $this->load->view('Header_view');
        $this->load->view('Catalogue_view', $data);
        $this->load->view('Footer_view');
    }

    public function manifspage($page=0){
        $this->load->model('Catalogue_model');
        $data['toutesLesManifs'] = $this->Catalogue_model->get_manif_par_page($page);

        $this->load->library('pagination');

        $config['base_url'] = base_url().'Catalogue/manifspage';
        $config['total_rows'] = $this->Catalogue_model->nbmanif();
        $config['per_page'] = 4;
        $config['num_links'] = 10;
        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();

        $this->load->view('Header_view');
        $this->load->view('Catalogue_view', $data);
        $this->load->view('Footer_view');
    }


    public function pdf(){
        $this->load->model('Catalogue_model');
        $recup = $this->Catalogue_model->recuptous();
        $data['tous']=$recup;
        $this->load->view('Manifpdf_view', $data);
    }

    public function DataGraph($manifs=1){
        $this->load->model("Catalogue_model");
        $data["mesinfos"] = $this->Catalogue_model->infos("$manifs");
        $this->load->view("graph_view", $data);
    }

    public function legraph($manifs=1){
        $data['manifs']=$manifs;
        $this->load->view("legraph_view", $data);
    }
}
