<?php

class Salles extends CI_Controller {

    public function index()
    {
        $this->load->model('Salle_model');
        $recup = $this->Salle_model->recuptous();
        $data['tous']=$recup;
        $this->load->view('Header_view');
        $this->load->view('Salles_view', $data);
        $this->load->view('Footer_view');
    }

}