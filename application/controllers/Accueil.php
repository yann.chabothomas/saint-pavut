<?php
/**
 * Created by PhpStorm.
 * User: yannchabot-thomas
 * Date: 01/12/2018
 * Time: 18:16
 */

class Accueil extends CI_Controller {

    public function index()
    {
        $this->load->view('Header_view');
        $this->load->view('Accueil_view');
        $this->load->view('Footer_view');
    }
}