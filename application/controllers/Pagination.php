<?php
/**
 * Created by PhpStorm.
 * User: yannchabot-thomas
 * Date: 26/11/2018
 * Time: 14:47
 */

class Pagination extends CI_Controller{

    public function index()
    {
        $this->load->model('Pagination_model');
        $recup = $this->Pagination_model->recuptous();
        $data['tous']=$recup;
        $this->load->view('Header_view');
        $this->load->view('Pagination_view', $data);
        $this->load->view('Footer_view');
    }

    public function manifspage($page=0){
        $this->load->model('Pagination_model');
        $data['toutesLesManifs'] = $this->Pagination_model->get_manif_par_page($page);

        $this->load->library('pagination');

        $config['base_url'] = base_url().'Pagination/manifspage';
        $config['total_rows'] = $this->Pagination_model->nbmanif();
        $config['per_page'] = 4;
        $config['num_links'] = 10;
        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();

        $this->load->view('Header_view');
        $this->load->view('Pagination_view', $data);
        $this->load->view('Footer_view');
    }


}