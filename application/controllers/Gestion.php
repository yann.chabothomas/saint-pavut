<?php
/**
 * Created by PhpStorm.
 * User: yannchabot-thomas
 * Date: 04/12/2018
 * Time: 11:12
 */

class Gestion extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('grocery_CRUD');
        //$this->load->library('grocery_CRUD_Multiuploader');
        if (empty($_SESSION['ident']) OR $_SESSION['ident']!='admin') redirect(base_url().'Login');
    }

    public function index()
    {
        $this->load->view('Header_view');
        $this->load->view('Gestion_view');
        $this->load->view('Footer_view');
    }

    public function salles(){
        $crud = new grocery_CRUD();
        $crud->set_theme('flexigrid');
        $crud->set_table('salle');
        $crud->set_subject('une salle');
        $crud->unset_export();
        $crud->unset_print();
        $crud->unset_clone();
        $output = $crud->render();
        $this->load->view('Header_view', (array)$output);
        $this->load->view('Gestion_view', (array)$output);
        $this->load->view('Footer_view', (array)$output);
    }

    public function manifs(){
        $crud = new grocery_CRUD();
        $crud->set_theme('flexigrid');
        $crud->display_as('salle_code','Salle code');
        $crud->set_table('manifs');
        $crud->set_relation('salle_code','salle','salle_code');
        $crud->set_subject('une manifestation');
        $crud->display_as('salle_code','Salle Concernée');
        $crud->set_field_upload('manifs_photo','assets/img/photos', 'jpg|png');
        $crud->unset_export();
        $crud->unset_print();
        $crud->unset_clone();
        $crud->unset_columns('manifs_id');
        $output = $crud->render();
        $this->load->view('Header_view', (array)$output);
        $this->load->view('Gestion_view', (array)$output);
        $this->load->view('Footer_view', (array)$output);
    }

/*    public function multiupload(){
        $crud = new Grocery_CRUD_Multiuploader();
        $crud->set_theme('flexigrid');
        $crud->display_as('salleCode','Salle code');
        $crud->set_relation('salleCode','salle','salle_code');
        $crud->set_subject('une manifestation');
        $crud->display_as('salleCode','Choisir une salle');
        $crud->required_fields('manifs_photo');
        $crud->set_field_upload('file_url','assets/img/photos');
        $crud->unset_export();
        $crud->unset_print();
        $crud->unset_clone();
        $crud->unset_columns('manifs_id');
        $crud->set_table('manifs');
        $crud->required_fields('manifs_photo');
        $crud->set_field_upload('image','assets/img/photos');
        $crud->where('manifs_photo', 2);
        $config = array(
            /* Destination directory
            "path_to_directory" =>'assets/img/photos',

            /* Allowed upload type
            "allowed_types" =>'gif|jpeg|jpg|png',

            /* Show allowed file types while editing ?
            "show_allowed_types" => true,

            /* No file text
            "no_file_text" =>'No Pictures',

            /* enable full path or not for anchor during list state
            "enable_full_path" => false,

            /* Download button will appear during read state
            "enable_download_button" => true,

            /* One can restrict this button for specific types...
            "download_allowed" => 'jpg'
        );

        $crud->new_multi_upload("manifs_photo",$config);
        $output = $crud->render();
        $this->load->view('Header_view', (array)$output);
        $this->load->view('Gestion_view', (array)$output);
        $this->load->view('Footer_view', (array)$output);
    }
*/
    public function abonnes(){
        $crud = new grocery_CRUD();
        $crud->set_theme('flexigrid');
        $crud->set_table('abonnes');
        $crud->set_subject('un abonné');
        $crud->unset_export();
        $crud->unset_print();
        $crud->unset_clone();
        $crud->unset_columns('abo_id');
        $output = $crud->render();
        $this->load->view('Header_view', (array)$output);
        $this->load->view('Gestion_view', (array)$output);
        $this->load->view('Footer_view', (array)$output);
    }
}