<?php
/**
 * Created by PhpStorm.
 * User: yannchabot-thomas
 * Date: 10/12/2018
 * Time: 09:47
 */

class Login extends CI_Controller{

    public function index(){
        //$_SESSION['ident'] ="" ;
        $this->load->view('Header_view');
        $this->load->view('Login_view');
        $this->load->view('Footer_view');
    }

    public function Verif()
    {

        $ident = $this->input->post('username');
        $mdp = $this->input->post('password');
        $this->load->database();
        $sql = $this->db->get('connexion')->row();
        $data['login'] = $sql;
        foreach ($data as $connexion) {
            if ($connexion->username === $ident && $connexion->password === $mdp) {
                $_SESSION['ident'] = 'admin';
                redirect(base_url() . 'Gestion');
            } else {
                redirect(base_url() . 'Login');
            }
        }
    }


            /*     try {
                      $bdd = new PDO('mysql:host=localhost;dbname=pavut;charset=utf8', 'root', 'root');
                  } catch (Exception $e) {
                      die('Erreur : ' . $e->getMessage());
                  }
                  $req = $bdd->prepare('SELECT COUNT(*) AS connexion FROM pavut WHERE password = :password AND login = :login');
                  $req->bindValue(':password', $_POST['password']);
                  $req->bindValue(':login', $_POST['login']);
                  $data = $req->execute();
                  $data = $req->fetch();
                  $req->closeCursor();
                  if ($data['membre_valide'] = !0) //isset ou pas, le résultat est le même
                  {
                      redirect(base_url() . 'Gestion');
                  } else {
                      echo 'Mauvais login et/ou mot de passe';
                  }

              }

                  /*if(isset($_POST['connexion'])){
                      $bdd = new PDO ('mysql:host=localhost;dbname=pavut;charset=utf8', 'root', 'root');
                      $identifiant=$_POST['identifiant'];
                      $password=$_POST['password'];
                      if($identifiant&&$password){
                          $query = $bdd->query('SELECT * FROM connexion WHERE login = "$identifiant" AND mdp = "$password"');
                          $rows = $query->fetch();
                          if ($rows == 1){
                              redirect(base_url().'Gestion');
                          }else {
                              redirect(base_url().'Login');
                          }

                      }
                  }
              }

                  $ident = $this->input->post('username');
                  $mdp = $this->input->post('password');
                  if ($ident=="admin" && $mdp=="m3202"){
                      $_SESSION['ident'] = 'admin';
                      redirect(base_url().'Gestion');
                  } else {
                      redirect(base_url().'Login');
                  } */



    public function Deco(){
        session_destroy();
        redirect(base_url().'Login');
    }
}