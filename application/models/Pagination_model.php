<?php
/**
 * Created by PhpStorm.
 * User: yannchabot-thomas
 * Date: 26/11/2018
 * Time: 14:52
 */

class Pagination_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function toutes(){
        $query = $this->db->get('manifs');
        return $query->result();
    }

    public function get_manif_par_page($page){
        $query = $this->db->get('manifs',4, $page);
        return $query->result();
    }

    public function nbmanif(){
        return $this->db->count_all('manifs');
    }

}