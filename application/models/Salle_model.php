<?php
/**
 * Created by PhpStorm.
 * User: yannchabot-thomas
 * Date: 23/11/2018
 * Time: 11:06
 */

class Salle_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function recuptous(){
        $query = $this->db->query('SELECT * FROM salle');
        return $query->result();
    }

}