<?php
/**
 * Created by PhpStorm.
 * User: yannchabot-thomas
 * Date: 21/11/2018
 * Time: 12:14
 */

class Catalogue_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function recuptous(){
        $query = $this->db->query('SELECT manifs_intitule, manifs_photo, manifs_type, manifs_description , ROUND((manifs_prix_place/0.88), 2) '.' AS manifs_prix_place FROM manifs');
        return $query->result();
    }

    public function toutes(){
        $query = $this->db->get('manifs');
        return $query->result();
    }

    public function get_manif_par_page($page){
        $query = $this->db->get('manifs',4, $page);
        return $query->result();
    }

    public function nbmanif(){
        return $this->db->count_all('manifs');
    }

    public function infos($p){
        $graph = $this->db->query("
        SELECT abo_ville, SUM(abo_qte_place_reserv) AS totalresaabo , manifs_intitule
        FROM manifs
        INNER JOIN reservations ON manifs.manifs_id = reservations.manifs_id
        INNER JOIN abonnes ON abonnes.abo_id = reservations.abo_id
        WHERE manifs.manifs_id = $p GROUP BY abo_ville");
        return $graph->result();
    }
}
