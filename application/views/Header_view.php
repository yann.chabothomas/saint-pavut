<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="M3202 - PHP">
        <meta name="author" content="Yann Chabot-Thomas">

        <title>Saint Pavut</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url() ?>/assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url() ?>/assets/css/half-slider.css" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/styles.css">
        <?php   if (isset($css_files)):
            echo 'css';
        foreach ($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>">
        <?php endforeach;
        endif;?>

    </head>

    <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#">Saint Pavut</a class="navbar-brand">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="header" href="<?php echo base_url(); ?>Accueil">Home</a>
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="header" href="<?php echo base_url(); ?>Catalogue">Catalogue</a>
                    </li>
                    <li class="nav-item">
                        <a class="header" href="<?php echo base_url(); ?>Salles">Salles</a>
                    </li>
                    <li class="nav-item">
                        <a class="header" href="<?php echo base_url(); ?>Login">Administration</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class="carousel-item active" style="background-image: url('https://cms.hostelworld.com/hwblog/wp-content/uploads/sites/2/2017/10/alexandre-godreau-188906.jpg')">
                </div>
                <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class="carousel-item" style="background-image: url('https://cms.hostelworld.com/hwblog/wp-content/uploads/sites/2/2017/10/dan-gribbin-50368.jpg')">
                </div>
                <!-- Slide Three - Set the background image for this slide in the line below -->
                <div class="carousel-item" style="background-image: url('https://cms.hostelworld.com/hwblog/wp-content/uploads/sites/2/2017/10/andrew-gook.jpg')">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </header>




