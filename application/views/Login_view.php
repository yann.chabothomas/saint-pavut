<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="login-block">
    <div class="containers">
        <div class="row ">
            <div class="col login-sec">
                <h2 class="text-center">Login Now</h2>
                <form class="login-form" method="post" action="<?= base_url().'Login/Verif'?>">
                    <div class="form-group">
                        <label for="username" class="text-uppercase">Identifiant</label>
                        <input type="text" name="username" class="form-control" placeholder="" id="username">
                    </div>
                    <div class="form-group">
                        <label for="password" class="text-uppercase">Mot de Passe</label>
                        <input type="password" name="password" class="form-control" placeholder="" id="password">
                    </div>
                    <div class="form-check">
                        <button type="submit" name="submit" class="btn btn-login float-right">Connexion</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>