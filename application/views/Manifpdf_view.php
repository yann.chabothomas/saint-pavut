<?php

require_once('assets/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetTitle('Catalogue des manifestations');
// set default header data

$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->AddPAge();

$montext = '<h1>Bienvenu sur St Pavut</h1>
                <h2>Voici notre catalogue de manifestations</h2>';

$pdf->Image('assets/img/photos/after02.jpg', 30, 65, 150, 190, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);

$pdf->writeHTML($montext);

$pdf->AddPage();

foreach ($tous as $manifs){
    $pdf->writeHTML( '<img src="'.base_url().'/assets/img/photos/'.$manifs->manifs_photo.'"/>'."<br>");
    $pdf->writeHTML( $manifs->manifs_type. "<br>");
    $pdf->writeHTML( $manifs->manifs_intitule. "<br>");
    $pdf->writeHTML( $manifs->manifs_description. "<br>");
    $pdf->writeHTML( $manifs->manifs_prix_place.' $'."<br>");
    $pdf->AddPage();
}

$pdf->deletePage(22);

$pdf->Output('stpavut.pdf');

?>