<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


                <!-- Content -->
                <div id="content">
                    <section id="manifestation">
                    <h2><br>Salles :<br></h2>
                    <div class="truc">
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Code</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Place maximum</th>
                                <th scope="col">Année de création</th>
                                <th scope="col">Prix location (€)</th>
                                <th scope="col">Surface (m²)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($tous as $salle) { ?>
                                <tr>
                                    <div class="colonne">
                                        <td scope="col"><?php echo $salle->salle_code; ?></td>
                                        <td scope="col"><?php echo $salle->salle_nom; ?></td>
                                        <td scope="col"><?php echo $salle->salle_place_max; ?></td>
                                        <td scope="col"><?php echo $salle->salle_date_creation; ?></td>
                                        <td scope="col"><?php echo $salle->salle_prix_loc; ?></td>
                                        <td scope="col"><?php echo $salle->salle_surface; ?></td>
                                    </div>
                                </tr>

                            <?php  } ?>

                            </tbody>
                        </table>
                </div>
            </div>
        </div>