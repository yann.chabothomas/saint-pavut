<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Content -->
<div id="content">
    <section id="manifestation">
        <h2><br>Manifestations :<br></h2>
        <div class="truc">

            <?php foreach ($toutesLesManifs as $manifs) { ?>

                <div class="items">
                    <img src="<?php echo base_url(); ?>/asset/img/photos/<?= $manifs->manifs_photo ?>" alt="<?= $manifs->manifs_photo ?>">
                    <div class="colonne">
                        <p><?php echo $manifs->manifs_type; ?></p>
                        <p><?php echo $manifs->manifs_intitule; ?></p>
                        <p><?php echo $manifs->manifs_description; ?></p>
                        <p><?php echo $manifs->manifs_prix_place.' $'; ?></p>
                    </div>

                </div>

            <?php  } ?>
        </div>
        <div id="pagination">
            <?php echo $pagination; ?>
        </div>
    </section>
</div>
</div>
</div>