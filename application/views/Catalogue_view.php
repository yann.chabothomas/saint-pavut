<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Content -->
<div id="content">
    <section id="manifestation">
        <h2><br>Manifestations : <a class="pdf" href="<?php echo base_url(); ?>Catalogue/pdf" target="_blank">Catalogue des manifestations</a><br></h2>
        <div class="truc">

            <?php foreach ($toutesLesManifs as $manifs) { ?>

                <div class="items">
                    <img src="<?php echo base_url(); ?>/assets/img/photos/<?= $manifs->manifs_photo ?>" alt="<?= $manifs->manifs_photo ?>">
                    <div class="colonne">
                        <p><strong>Type de manifestation : </strong><?php echo $manifs->manifs_type; ?></p>
                        <p><strong>Nom de la manifestation : </strong><?php echo $manifs->manifs_intitule; ?></p>
                        <p><strong>Description : </strong><?php echo $manifs->manifs_description; ?></p>
                        <p><strong>Prix de la place : </strong><?php echo $manifs->manifs_prix_place.' $'; ?></p>
                        <a style="text-decoration: underline" href="<?php echo base_url() ?>Catalogue/legraph/<?php echo $manifs->manifs_id ?>" target="wclose" onclick="window.open('<?php echo base_url() ?>Catalogue/legraph/<?php echo $manifs->manifs_id ?>','wclose', 'width=600,height=570,toolbar=no,status=no,left=20,top=30')">Voir le graphique</a>
                    </div>

                </div>

            <?php  } ?>
        </div>
        <div id="pagination">
            <?php echo $pagination; ?>
        </div>
    </section>
</div>
</div>
</div>