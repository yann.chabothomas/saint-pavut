<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Graphique</title>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        console.log("go");
        google.charts.load('current', {'packages' :['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart(){
            var data = new google.visualization.DataTable();
            var recupdatas = $.ajax({url:"<?php echo base_url() ?>Catalogue/datagraph/<?php echo $manifs ?>", datatype:"json", async:false}).responseText;
            var data = new google.visualization.DataTable(recupdatas);
            var options = {'title':'Graphique n°<?php echo $manifs ?>','height':400 ,is3D:true};
            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
    </script>
</head>
<body>
    <div id="chart_div"></div>

</body>
</html>