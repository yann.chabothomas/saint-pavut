<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="py-5">
    <div class="container">
        <h1>Bienvenue sur le back office de Saint Pavut</h1>
        <div class="liens">
            <a href="<?php echo base_url() ?>/Gestion/salles">Les Salles</a>
            <a href="<?php echo base_url() ?>/Gestion/abonnes">Les Abonnées</a>
            <a href="<?php echo base_url() ?>/Gestion/manifs">Les Manifestations</a>
            <a href="<?php echo base_url() ?>/Login/Deco">Déconnexion</a>
        </div>
    </div>
    <div class="centrer">
        <?php echo $output; ?>
    </div>
</section>